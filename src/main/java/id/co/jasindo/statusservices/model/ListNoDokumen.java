package id.co.jasindo.statusservices.model;

import java.io.Serializable;
import java.util.ArrayList;

import javassist.expr.NewArray;

public class ListNoDokumen implements Serializable{

	private static final long serialVersionUID = -50230365451111L;
	
	ArrayList<String> noDokumenList = new ArrayList<String>();

	public ArrayList<String> getNoDokumenList() {
		return noDokumenList;
	}

	public void setNoDokumenList(ArrayList<String> noDokumenList) {
		this.noDokumenList = noDokumenList;
	}
	
	
}
