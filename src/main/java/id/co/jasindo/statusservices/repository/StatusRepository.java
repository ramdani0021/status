package id.co.jasindo.statusservices.repository;


import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.jasindo.statusservices.model.StatusModel;

public interface StatusRepository extends JpaRepository<StatusModel, Long> {

	
	@Query(value = "SELECT * FROM STATUS "
			+ "WHERE NO_DOKUMEN IN (:noDokumen)", nativeQuery = true)
    public List<StatusModel> getDatabyNoDokumen(
            @Param("noDokumen") ArrayList<String> noDokumen
    );
	
	StatusModel findByNoDokumen(String noDokumen);
}
