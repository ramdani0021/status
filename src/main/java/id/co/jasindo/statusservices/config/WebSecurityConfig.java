package id.co.jasindo.statusservices.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {    	
    	 http.csrf().disable()
         .authorizeRequests()
         .antMatchers("/user/**").hasRole("USER")
         .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
         ;


    }

    @Override
    @Autowired
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
    	  auth
          .inMemoryAuthentication()
          .withUser("super").password(encoder().encode("super")).roles("ADMIN")
          .and()
          .withUser("user").password(encoder().encode("user")).roles("USER");
 }
    
    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }
    
//    @Bean
//    public AuthenticationEntryPoint getBasicAuthEntryPoint(){
//        return new AuthenticationEntryPoint();
//    }

    /* To allow Pre-flight [OPTIONS] request from browser */
    @Override
    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
    	
    }

}