package id.co.jasindo.statusservices.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.audit4j.core.handler.ConsoleAuditHandler;
import org.audit4j.core.handler.Handler;
import org.audit4j.core.MetaData;
import org.audit4j.core.handler.file.FileAuditHandler;
import org.audit4j.core.layout.Layout;
import org.audit4j.core.layout.SimpleLayout;
import org.audit4j.handler.db.DatabaseAuditHandler;
import org.audit4j.integration.spring.AuditAspect;
import org.audit4j.integration.spring.SpringAudit4jConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;




@Configuration
@EnableAspectJAutoProxy
public class AuditConfig {

	 @Value("${spring.datasource.url}")
	 private String urlDb;
	 
	 @Value("${spring.datasource.username}")
	 private String username;
	 
	 @Value("${spring.datasource.driver-class-name}")
	 private String driver;
	 
	 @Value("${spring.datasource.password}")
	 private String password;

	
    @Bean
    public Layout layout() {
       return new SimpleLayout();
    }

    @Bean
    public MetaData metaData() {
        return new MyMetaData();
    }

    @Bean
    public DatabaseAuditHandler databaseAuditHandler(){
        DatabaseAuditHandler databaseAuditHandler = new DatabaseAuditHandler();
        databaseAuditHandler.setEmbedded("false");
        databaseAuditHandler.setDb_user(username);
        databaseAuditHandler.setDb_password(password);
        databaseAuditHandler.setDb_url(urlDb);
        databaseAuditHandler.setDb_driver(driver);
        return databaseAuditHandler;
    }

//    @Bean
//    public FileAuditHandler fileAuditHandler(){
//        FileAuditHandler fileAuditHandler = new FileAuditHandler();
//        return fileAuditHandler;
//    }
//
//    @Bean
//    public ConsoleAuditHandler consoleAuditHandler(){
//        return new ConsoleAuditHandler();
//    }

    private HashMap<String,String> getProperties() {
        HashMap<String,String> properties = new HashMap<>();

        properties.put("log.file.location", ".");

        return properties;
    }
    
    @Bean
    public SpringAudit4jConfig springAudit4jConfig(){
        SpringAudit4jConfig audit4jConfig = new SpringAudit4jConfig();
        audit4jConfig.setLayout(new SimpleLayout());
        List<Handler> handlers = new ArrayList<>();
//        handlers.add(new ConsoleAuditHandler());
//        handlers.add(new FileAuditHandler());
        handlers.add(databaseAuditHandler());

        audit4jConfig.setHandlers(handlers);
        audit4jConfig.setMetaData(new MyMetaData());
        audit4jConfig.setProperties(getProperties());

        return audit4jConfig;
    }


    @Bean
    public AuditAspect auditAspect() {
        return new AuditAspect();
    }
}