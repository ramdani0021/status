package id.co.jasindo.statusservices.controller;


import io.swagger.v3.oas.annotations.tags.Tag;
import net.sf.json.JSONObject;

import org.audit4j.core.annotation.Audit;
import org.audit4j.core.annotation.AuditField;
//import org.audit4j.core.annotation.Audit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import id.co.jasindo.statusservices.model.GeneralResponse;
import id.co.jasindo.statusservices.model.Status;
import id.co.jasindo.statusservices.model.StatusModel;
import id.co.jasindo.statusservices.repository.StatusRepository;
import id.co.jasindo.statusservices.service.StatusServices;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;


@Tag(name = "Log Status API", 
	description = "Provides Log Status API's.")

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(path = "/status")
public class StatusController {
	@Autowired
	StatusRepository statusRepository;
	
	@Autowired
	StatusServices statusServices;
	
	ArrayList<String> noDokumenList = new ArrayList<String>();

	@Audit
	@PostMapping
	public ResponseEntity<?> simpanData(@RequestBody @AuditField(field="saveStatus") StatusModel statusModel){
		JSONObject response = new JSONObject();
		HttpStatus restResponse = HttpStatus.CREATED;    
		try {
	            String noDokumen = statusServices.createData(statusModel);
	            response.put("Status", Status.SUCCESS);
	            response.put("Message", "Data Berhasil Disimpan");
	            response.put("noDokumen", noDokumen);
	        }catch (Exception e){
	        	response.put("Status", Status.FAILED);
	            response.put("Message", e.getMessage());
	            restResponse = HttpStatus.BAD_REQUEST;
	        }
	      return new ResponseEntity<>(response, restResponse);
	}
	
	@Audit
	@GetMapping(value = "/noDokumenList")
	public ResponseEntity<?> getbyNoDokumenList(@RequestParam @AuditField(field="noDokumenList") ArrayList<String> listDokumen) {
		GeneralResponse response = new GeneralResponse();
		HttpStatus restResponse = HttpStatus.OK;
		try {
			List<StatusModel> listDataStatus = statusRepository.getDatabyNoDokumen(listDokumen);
			if (listDataStatus == null) {
				restResponse =  HttpStatus.NOT_FOUND;
			}
			response.setStatus(Status.SUCCESS);
            response.setData(listDataStatus);
		} catch (Exception e) {
			response.setStatus(Status.FAILED);
            response.setMessage(e.getMessage());
            restResponse = HttpStatus.BAD_REQUEST;
        }
      return new ResponseEntity<>(response, restResponse);
	}
	
	@Audit
	@GetMapping(value = "/noDokumen")
	public ResponseEntity<?> getbyNoDokumen(@RequestParam @AuditField(field="noDokumen")String noDokumen) {
		GeneralResponse response = new GeneralResponse();
		HttpStatus restResponse = HttpStatus.OK;
		try {
			StatusModel listDataStatus = statusRepository.findByNoDokumen(noDokumen);
			if (listDataStatus == null) {
				restResponse =  HttpStatus.NOT_FOUND;
			}
			response.setStatus(Status.SUCCESS);
            response.setData(listDataStatus);
		} catch (Exception e) {
			response.setStatus(Status.FAILED);
            response.setMessage(e.getMessage());
            restResponse = HttpStatus.BAD_REQUEST;
        }
      return new ResponseEntity<>(response, restResponse);
	}

	@Audit
	@GetMapping
	public ResponseEntity<?> getAll(){
		GeneralResponse response = new GeneralResponse();
		HttpStatus restResponse = HttpStatus.OK;
		try {
			List<StatusModel> listDataStatus = statusRepository.findAll();	
			if (listDataStatus.isEmpty()) {
				restResponse =  HttpStatus.NOT_FOUND;
			}
			response.setStatus(Status.SUCCESS);
            response.setData(listDataStatus);
		} catch (Exception e) {
			response.setStatus(Status.FAILED);
            response.setMessage(e.getMessage());
            restResponse = HttpStatus.BAD_REQUEST;
        }
      return new ResponseEntity<>(response, restResponse);
	}
	
	 @Audit
	 @PutMapping(value = "/{id}")
	    public ResponseEntity<?> updateData(
	    		@PathVariable @AuditField(field="id") Long id, 
	    		@RequestBody @AuditField(field="data") StatusModel statusModel) {
			JSONObject response = new JSONObject();
			HttpStatus restResponse = HttpStatus.OK;
			try {
				String noDokumen = statusServices.updateData(id, statusModel);
				response.put("Status", Status.SUCCESS);
				response.put("Message", "Data Berhasil Diupdate");
	            response.put("noDokumen", noDokumen);
	            restResponse = HttpStatus.CREATED;
	        }catch (Exception e){
	        	response.put("Status", Status.FAILED);
	            response.put("Message", e.getMessage());
	            restResponse = HttpStatus.BAD_REQUEST;
	        }
	      return new ResponseEntity<>(response, restResponse);
	    }
	 
	 @Transactional
	 @Audit
	 @DeleteMapping(value = "/{id}")
	    public ResponseEntity<?> hapusData(
	    		@PathVariable @AuditField(field = "id") Long id){
			JSONObject response = new JSONObject();
			HttpStatus restResponse = HttpStatus.OK;
			try {
		    statusRepository.deleteById(id);
		    response.put("Status", Status.SUCCESS);
			response.put("Message", "Data Berhasil DiHapus");
		} catch (Exception e) {
			response.put("Status", Status.FAILED);
            response.put("Message", e.getMessage());
            restResponse = HttpStatus.BAD_REQUEST;
		}
		 return new ResponseEntity<>(response, restResponse);
	    }

}
	