-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2020 at 09:53 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_jasindo`
--

-- --------------------------------------------------------

--
-- Table structure for table `audit`
--

CREATE TABLE `audit` (
  `identifier` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `actor` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `origin` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL,
  `action` varchar(200) COLLATE utf8mb4_bin NOT NULL,
  `elements` longtext COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `audit`
--

INSERT INTO `audit` (`identifier`, `timestamp`, `actor`, `origin`, `action`, `elements`) VALUES
('3345341216247532748', '2020-07-09 02:58:36', 'user', '0:0:0:0:0:0:0:1', 'getAll', ''),
('310169584656467635', '2020-07-09 03:24:47', 'anonymous', '0:0:0:0:0:0:0:1', 'getAll', ''),
('-652717494315169578', '2020-07-09 03:27:47', 'anonymous', '0:0:0:0:0:0:0:1', 'getAll', ''),
('-170886355799554100', '2020-07-09 03:42:15', 'super', '0:0:0:0:0:0:0:1', 'getAll', ''),
('-8315997122532455523', '2020-07-09 04:39:51', 'anonymous', '0:0:0:0:0:0:0:1', 'getAll', ''),
('-5778387011988209171', '2020-07-09 04:40:47', 'anonymous', '0:0:0:0:0:0:0:1', 'getAll', ''),
('2571801095840678912', '2020-07-09 05:32:25', 'anonymous', '0:0:0:0:0:0:0:1', 'getAll', ''),
('-1395562239484082773', '2020-07-09 06:14:54', 'anonymous', '0:0:0:0:0:0:0:1', 'getAll', ''),
('6653396614230983374', '2020-07-09 06:18:07', 'anonymous', '0:0:0:0:0:0:0:1', 'simpanData', 'saveStatus$id.co.jasindo.statusservices.model.StatusModel$id java.lang.Long:1, saveStatus$id.co.jasindo.statusservices.model.StatusModel$noDokumen java.lang.String:21011991, saveStatus$id.co.jasindo.statusservices.model.StatusModel$asal java.lang.String:string, saveStatus$id.co.jasindo.statusservices.model.StatusModel$status java.lang.Short:0, saveStatus$id.co.jasindo.statusservices.model.StatusModel$createdAt$java.util.Date$fastTime java.lang.Long:1594252800000, '),
('-3533341134237842208', '2020-07-09 06:19:26', 'anonymous', '0:0:0:0:0:0:0:1', 'updateData', 'id java.lang.Long:1, data$id.co.jasindo.statusservices.model.StatusModel$id java.lang.Long:0, data$id.co.jasindo.statusservices.model.StatusModel$noDokumen java.lang.String:210196, data$id.co.jasindo.statusservices.model.StatusModel$asal java.lang.String:bogor, data$id.co.jasindo.statusservices.model.StatusModel$status java.lang.Short:0, data$id.co.jasindo.statusservices.model.StatusModel$status_asal java.lang.Short:2, data$id.co.jasindo.statusservices.model.StatusModel$catatan java.lang.String:nothing impossible, data$id.co.jasindo.statusservices.model.StatusModel$user java.lang.String:ramdani, data$id.co.jasindo.statusservices.model.StatusModel$createdAt$java.util.Date$fastTime java.lang.Long:1594275536850, '),
('-804092674916987053', '2020-07-09 07:43:15', 'anonymous', '0:0:0:0:0:0:0:1', 'hapusData', 'arg1 java.lang.Long:7, '),
('4012168199494060038', '2020-07-09 07:44:46', 'anonymous', '0:0:0:0:0:0:0:1', 'hapusData', 'id java.lang.Long:3, ');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` bigint(20) NOT NULL,
  `asal` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `catatan` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `no_dokumen` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `status_asal` smallint(6) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `user` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `asal`, `catatan`, `created_at`, `no_dokumen`, `status`, `status_asal`, `updated_at`, `user`) VALUES
(2, 'LKS', 'OKE', '2020-06-26 13:35:13', '111111', 2, 1, '2020-06-26 13:35:13', 'ramdani');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
